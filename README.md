We teach children the joy and value of cooking. We have weekly culinary classes for kids ages 3-18. We also offer birthday parties, field trips and camps.

Address: 9690 Seminole Blvd, Seminole, FL 33772

Phone: 727-350-4587
